﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter Employee Name")]
        public string EmployeeName { get; set; }

        [Required(ErrorMessage = "Please choose profile image")]
        public string EmployeeImage { get; set; }

        // Navigation Properties
        public int? DepartmentId { get; set; }
        public Department Department { get; set; }
    }
}
